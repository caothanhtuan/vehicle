import * as localforage from 'localforage';
import { Injectable } from '@angular/core';
import { Observable, from } from 'rxjs';
import 'rxjs/add/observable/fromPromise';

@Injectable({
    providedIn: 'root'
})
export class StorageService {

  public setItem<T>(key: string, value: T): Observable<T> {
    return Observable.fromPromise(localforage.setItem(key, value));
  }

  public getItem<T>(key: string): Observable<T> {
    return Observable.fromPromise(localforage.getItem(key));
  }

  public removeItem(key: string): Observable<void> {
    return Observable.fromPromise(localforage.removeItem(key));
  }

  public clear(): Observable<void> {
    return Observable.fromPromise(localforage.clear());
  }
}
