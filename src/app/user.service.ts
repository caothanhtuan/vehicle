import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap, filter, switchMap } from 'rxjs/operators';

import { User, VehicleLocation } from './user';
import { MessageService } from './message.service';
import { CacheService } from './cache.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})

export class UserService {
  // URL to web api
  baseUrl = 'http://mobi.connectedcar360.net/api/?op=';
  usersUrl = this.baseUrl + 'list';
  locationUrl = this.baseUrl + 'getlocations&userid=';
  USER_CACHE_TIME = 5 * 60; // second
  LOCATION_CACHE_TIME = 30; // second
  USER_CACHE_KEY = 'Users';
  LOCATION_CACHE_KEY = 'Vehicle-Location-';
  ERROR_CANNOT_LOAD_USERS = 'Cannot load users, please try again!';
  ERROR_CANNOT_LOAD_LOCATION = 'Cannot load vehicle location, please try again!';

  constructor(
    private http: HttpClient,
    private messageService: MessageService,
    private cache: CacheService) { }

  /** GET users from the server */
  getUsers (): Observable<User[]> {
    // Cache an observable
    const request = this.http.get<User[]>(this.usersUrl)
                      .pipe(
                        map((res: any) => res.data),
                        // tap(data => this.log(`fetched users`)),
                        catchError(this.handleError(this.ERROR_CANNOT_LOAD_USERS, []))
                      );

    return this.cache.observable(this.USER_CACHE_KEY, request, this.USER_CACHE_TIME);
  }

  /** GET location of vehicles by user id */
  getVehicleLocations (id: number): Observable<VehicleLocation[]> {
    const cacheKey = this.LOCATION_CACHE_KEY + id;
    const request = this.http.get<VehicleLocation[]>(this.locationUrl + id)
                      .pipe(
                        map((res: any) => res.data),
                        // tap(data => this.log(`fetched vehicle locations`)),
                        catchError(this.handleError(this.ERROR_CANNOT_LOAD_LOCATION, []))
                      );

    return this.cache.observable(cacheKey, request, this.LOCATION_CACHE_TIME);
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (message: string, result?: T) {
    return (error: any): Observable<T> => {
      // TODO: better job of transforming error for user consumption
      // this.log(`${message} => ${error.message}`);
      this.log(`${message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

   /** Log a UserService message with the MessageService */
  private log(message: string) {
      this.messageService.clear();
      this.messageService.add(`${message}`);
  }

  clearUserCache(): void {
    this.cache.removeItem(this.USER_CACHE_KEY);
  }

  clearLocationCache(userid: string): void {
    const cacheKey = this.LOCATION_CACHE_KEY + userid;
    this.cache.removeItem(cacheKey);
  }
}
