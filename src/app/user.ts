export class User {
    userid: number;
    owner: Owner;
    vehicles: Vehicle[];
}

export class Owner {
    name: string;
    surname: string;
    foto: string;
}

export class Vehicle {
    vehicleid: number;
    make: string;
    model: string;
    year: string;
    color: string;
    vin: string;
    foto: string;
}

export class VehicleLocation {
    vehicleid: number;
    lat: number;
    lon: number;
}
