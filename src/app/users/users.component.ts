import { Component, OnInit } from '@angular/core';

import { User } from '../user';
import { UserService } from '../user.service';
import { MessageService } from '../message.service';
import { isEmpty } from 'lodash';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.sass']
})

export class UsersComponent implements OnInit {
  users: User[];

  constructor(
    private userService: UserService,
    private messageService: MessageService) { }

  ngOnInit() {
    this.messageService.clear();
    this.getUsers();
  }

  getUsers(): void {
    this.userService.getUsers()
        .subscribe(users => {
          this.users = users.filter(user => (user.userid !== undefined));
          if (isEmpty(this.users) || this.users.length === 0) {
            this.userService.clearUserCache();
          }
        });
  }
}
