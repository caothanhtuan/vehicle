/// <reference types="@types/googlemaps" />
import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { User, Vehicle, VehicleLocation } from '../user';
import { UserService } from '../user.service';
import { isEmpty } from 'lodash';

@Component({
  selector: 'app-vehicle-map',
  templateUrl: './vehicle-map.component.html',
  styleUrls: ['./vehicle-map.component.sass']
})
export class VehicleMapComponent implements OnInit {
  @ViewChild('gmap') gmapElement: any;
  map: google.maps.Map;

  user: User;
  vehicles: Vehicle[];
  vehicleLocations: VehicleLocation[];
  markers = new Object();

  ngOnInit() {
    const userid = +this.route.snapshot.paramMap.get('id');
    this.getUser(userid);
  }

  ngAfterContentInit() {
    const mapProp = {
      center: new google.maps.LatLng(56.9496, 24.1052), // riga
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    this.map = new google.maps.Map(this.gmapElement.nativeElement, mapProp);
  }

  constructor(
    private route: ActivatedRoute,
    private userService: UserService
  ) { }

  getUser(userid): void {
    this.userService.getUsers()
      .subscribe(users => {
        this.user = users.filter(user => (user.userid === userid))[0];
        if (!isEmpty(this.user)) {
          this.vehicles = this.user.vehicles;
          this.getVehicleLocation(this.user.userid);
        } else {
          this.userService.clearUserCache();
        }
      });
  }

  getVehicleLocation(userid): void {
    this.userService.getVehicleLocations(userid)
      .subscribe(locations => {
        this.vehicleLocations = locations;
        if (!isEmpty(this.vehicleLocations) && this.vehicleLocations.length !== 0) {
          this.markLoations();
        } else {
          this.userService.clearLocationCache(userid);
        }
      });
  }

  markLoations(): void {
    let marker, i;
    const bounds = new google.maps.LatLngBounds();

    for (i = 0; i < this.vehicleLocations.length; i++) {
      const vehicle = this.vehicles.find(v => v.vehicleid === this.vehicleLocations[i].vehicleid);
      const pinImage = {
        url: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|' + vehicle.color.replace('#', ''),
        size: new google.maps.Size(20, 32),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(0, 32)
      };

      marker = new google.maps.Marker({
        position: new google.maps.LatLng(this.vehicleLocations[i].lat, this.vehicleLocations[i].lon),
        map: this.map,
        icon: pinImage
      });

      this.markers[vehicle.vehicleid] = marker;
      const content = '<div class="tooltip">' +
                        '<img src="' + vehicle.foto + '" alt="Vehicle photo" />' +
                        '<div>' + vehicle.model + ': ' + this.vehicleLocations[i].lat + ', ' + this.vehicleLocations[i].lon + '</div>' +
                      '</div>';

      bounds.extend(marker.getPosition());
      this.map.fitBounds(bounds);

      google.maps.event.addListener(marker, 'click', (
        function(marker, content) {
          return function() {
              marker.setAnimation(google.maps.Animation.BOUNCE);

              const infowindow = new google.maps.InfoWindow();
              infowindow.setContent(content);
              infowindow.open(this.map, marker);

              setTimeout(function () {
                marker.setAnimation(null);
                infowindow.close();
              }, 3000);
           };
      })(marker, content));
    }
  }

  setCenter(vehicleid): void {
    const marker = this.markers[vehicleid];
    const position = marker.getPosition();
    this.map.setCenter(new google.maps.LatLng(position.lat(), position.lng()));
    google.maps.event.trigger(marker, 'click');
  }
}
